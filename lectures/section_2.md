# Logical Form and Logical Equivalence

| Symbol         | Meaning                            |
| -------------- | ---------------------------------- | --------- |
| $$\in$$        | Is a member of                     |
| $$             | $$                                 | Such that |
| :              | Such that (not used in this class) |
| $$\approx$$    | Is approximately                   |
| $$\therefore$$ | Therefore                          |
| $$\because$$   | Because                            |

| Set of Numbers     | Symbol                      | Description               |
| ------------------ | --------------------------- | ------------------------- | ----------------------------------------- |
| Natural Numbers    | $$\mathbb{N}$$              | $${1,2,3,...}$$           |
| Whole Numbers      | $$\mathbb{N} \cup {0}$$     | $${0,1,2,3,...}$$         |
| Integers           | $$\mathbb{Z}$$              | You know this.            |
| Rational Numbers   | $$\mathbb{Q}$$              | $${\frac{a}{b}            | a,b \in Z \land b \neq 0}$$               |
| Irrational Numbers | $$\mathbb{R} - \mathbb{Q}$$ | $${x \in \mathbb{R}       | x \notin \mathbb{Q}}$$                    |
| Real Numbers       | $$\mathbb{R}$$              | Everything but imaginary. |
| Complex Numbers    | $$\mathbb{C}$$              | $${a+bi                   | a,b \in \mathbb{R} \land i = \sqrt{-1}}$$ |

-   $$\mathbb{C}$$ contains
-   $$\mathbb{R}$$ contains
-   $$\mathbb{Q}$$ contains
-   $$\mathbb{Z}$$ contains
-   $$\mathbb{N}$$

Such verbal or written assertions are called propositions or statements.

A statement is a sentence that is true or false but not both.

## Compound statements

| Symbol     | Description  | Logical Expression |
| ---------- | ------------ | ------------------ |
| $$\sim$$   | Not          | Negation           |
| $$\lor$$   | inclusive or | disjunction        |
| $$\land$$  | and          | conjunction        |
| $$\oplus$$ | exclusive or | xor                |

-   The negation of $$P$$ is $$\sim P$$
-   The conjunction of $$P$$ and $$Q$$ is $$P \land Q$$
-   The disjunction of $$P$$ and $$Q$$ is $$P \lor Q$$

## Logical equivalence

We say that two statement forms are **logically equivalent** if, and only if,
they have identical truth values for each possible substitution of statements for their
statement variables.

If statement forms $$P$$ and $$Q$$ are **logically equivalent** then we note that $$P \equiv Q$$.

A **tautology** is a statement form that is always true regardless of the truth values
of the individual statements substituted for its statement values. It is denoted by $$\mathbf{t}$$.

A **contradiction** is a statement form that is always false regardless of the truth
values of the individual statements substituted for its statement variables. It is denoted
by $$\mathbf{c}$$.

| Name                                          | Examples                                                                                                                 |
| --------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| Commutative Laws                              | $$p \land q \equiv q \land p$$ and $$p \lor q \equiv q \lor p$$                                                          |
| Associative Laws                              | $$(p \land q) \land r \equiv p \land (q \land r)$$ and $$(p \lor q) \lor r \equiv p \lor (q \lor r)$$                    |
| Distributive Laws                             | $$p \land (q \lor r) \equiv (p \land q) \lor (p \land r)$$ and $$p \lor (q \land r) \equiv (p \lor q) \land (p \lor r)$$ |
| Identity Laws                                 | $$p \land \mathbf{t} \equiv p$$ and $$p \lor \mathbf{c} \equiv p$$                                                       |
| Negation Laws                                 | $$p \lor \sim p \equiv \mathbf{t}$$ and $$p \land \sim p \equiv \mathbf{c}$$                                             |
| Double Negative Laws                          | $$\sim (\sim p) \equiv p$$                                                                                               |
| Idempotent Laws                               | $$p \land p \equiv p$$ and $$p \lor p \equiv p$$                                                                         |
| Universal Bound Laws                          | $$p \lor \mathbf{t} \equiv \mathbf{t}$$ and $$p \land \mathbf{c} \equiv c$$                                              |
| De Morgan's Laws                              | $$\sim (p \land q) \equiv \sim p\lor \sim q$$ and $$\sim (p \lor q) \equiv \sim p \land \sim q$$                         |
| Absorption Laws                               | $$p \lor (p \land q) \equiv p$$ and $$p \land (p \lor q) \equiv p$$                                                      |
| Negation of $$\mathbf{t}$$ and $$\mathbf{c}$$ | $$\sim \mathbf{t}\equiv \mathbf{c}$$ and $$\sim \mathbf{c} \equiv \mathbf{t}$$                                           |
