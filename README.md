# Discrete Math Notes

The document text and structure is written in Markdown.

The math is written in $$\KaTeX$$.

Graphs are generated with function-plot.

Compile locally with `gitbook build . public`.
