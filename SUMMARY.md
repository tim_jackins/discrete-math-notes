# Table of contents

## Lectures
* [Section 2](lectures/section_2.md)

## Meta
* [Introduction](README.md)
* [Source&#x2197;](https://gitlab.com/tim_jackins/discrete-math-notes)
* [Markdown syntax&#x2197;](https://www.markdownguide.org/basic-syntax/)
* [KaTeX supported functions&#x2197;](https://katex.org/docs/supported.html)
* [Function plot examples&#x2197;](https://mauriciopoppe.github.io/function-plot)
* [Function plot options&#x2197;](https://mauriciopoppe.github.io/function-plot/docs/interfaces/_src_types_.functionplotoptions.html)
